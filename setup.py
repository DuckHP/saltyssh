from setuptools import setup

setup(
    name='saltyssh',
    version='1.6',
    packages=['saltyssh'],
    package_dir={'saltyssh': 'src/saltyssh'},
    package_data={'saltyssh': ['docs/saltyssh.ini']},
    url='https://gitlab.com/DuckHP/saltyssh',
    license='GNU Lesser General Public License v3.0',
    author='saltyssh devs',
    description='An SSH tarpit',
    keywords=['honeypot', 'ssh', 'network', 'internet'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Framework :: AsyncIO',
        'Intended Audience :: System Administrators',
        'Intended Audience :: Information Technology',
        'Intended Audience :: Other Audience',
        'Intended Audience :: Telecommunications Industry',
        'License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.9',
        'Topic :: Communications :: BBS',
        'Topic :: Internet',
        'Topic :: Other/Nonlisted Topic',
        'Topic :: Security',
        'Topic :: System :: Networking',
        'Topic :: System :: Networking :: Monitoring'],
    entry_points={
        'console_scripts': [
            'saltyssh=saltyssh'
        ]

    },
    install_requires=['asyncio', 'urllib', 'configparser'],
    setup_requires=['flake8'],
)
