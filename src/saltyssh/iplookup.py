## use '{ip}' as template where
# the ip to lookup will be inserted
# ;geoip_url='freegeoip.net/xml/{ip}'
# ;geoip_url='https://ip2c.org/?ip={ip}'
# ;geoip_url='https://api.hackertarget.com/geoip/?q={ip}'

import subprocess
from urllib import error
from urllib import request

from saltyssh import GEOIP_LOOKUP_LOCAL_BIN_PATHS, \
    GEOIP_LOOKUP_LOCAL_BIN, HTTP_USER_AGENT


def geoIPlookup_Local(ip: str, url_template=None):
    """tries to find and use '/usr/bin/geoiplookup' in the form of 'geoiplookup 1.1.1.1'"""
    # cmd_line = GEOIP_LOOKUP_LOCAL_BIN_PATHS[0] + '/' + GEOIP_LOOKUP_LOCAL_BIN
    result = subprocess.run(['{path}/{cmd}'.format(cmd=GEOIP_LOOKUP_LOCAL_BIN,
                                                   path=GEOIP_LOOKUP_LOCAL_BIN_PATHS[0]),
                             '{ip}'.format(ip=ip)], stdout=subprocess.PIPE,
                            timeout=1)

    if result.returncode == 0:
        country = str(result.stdout, encoding='UTF-8').strip('\n').split(': ')[1][3:].lstrip(' ')
        if 'Address not found' in country:
            return 'NONE'
        else:
            return country
    else:
        if not country:
            return 'NONE'


def geoIPlookup_IP2C(ip:str, url_template: str = ''):
    # todo: implement being able to use alternative lookup sites from .ini config
    urlstring = 'https://ip2c.org/?ip={ip}'.format(ip=ip)

    jallaballa = request.Request(
        urlstring,
        data=None,
        headers={
            'User-Agent': HTTP_USER_AGENT}
    )

    # print(urlstring)
    try:
        with request.urlopen(jallaballa, timeout=5) as response:
            data = response.read()
            response.close()
    except error.URLError:
        return str("N/A")

    try:
        country = data.decode().split(';')[-1]
    except IndexError:
        country = "N/A"
    # print(data)
    return str(country)


#### THIS REQUIRE REGISTRATION/MEMBERSHIP TO DO MORE THAN A FEW LOOKUPS ####
def geoIPlookup_HackerTarget(ip, url_template: str = ''):
    # todo: implement being able to use alternative lookup sites from .ini config
    urlstring = 'https://api.hackertarget.com/geoip/?q={ip}'.format(ip=ip)
    # print(urlstring)
    with request.urlopen(urlstring) as response:
        data = response.read()
        response.close()
    data = data.decode("utf-8").split('\n')
    try:
        country = data[1].split(':')[1].lstrip(' ')
    except IndexError:
        country = "N/A"
    return str(country)

# geoIPlookup=geoIPlookup_IP2C
# geoIPlookup=geoIPlookup_HackerTarget
geoIPlookup=geoIPlookup_Local
#print(geoIPlookup('112.85.42.15'))
