import argparse
import logging

log_levels={
    'VERBOSE':19,
    'INFO':20,
    'CONNECT':20,
    'DISCONNECT':20,
}

# server_logger.setLevel('DEBUG')
# server_logger.debug('debug')
# server_logger.verbose('verbose')
# server_logger.info('info')
# server_logger.warning('warn')
# server_logger.critical('crit')

def log_verbose(self, message, *args, **kws):
    if self.isEnabledFor(log_levels['VERBOSE']):
        # Yes, logger takes its '*args' as 'args'.
        self._log(log_levels['VERBOSE'], message, args, **kws)

def makeLogger(arguments:argparse.Namespace=None):
    """create and configure logger(s)"""
    console_logger = logging.getLogger('saltyssh')

    stream_handler = logging.StreamHandler()

    stream_handler.setFormatter( logging.Formatter('%(asctime)s|%(levelname)s|%(message)s') )
    console_logger.handlers = [stream_handler]

    logging.addLevelName(log_levels['VERBOSE'], "VERBOSE")
    logging.Logger.verbose = log_verbose

    if arguments.verbose:
        console_logger.setLevel('INFO')
    elif arguments.verbose==1:
        console_logger.setLevel('VERBOSE')
    elif arguments.verbose==2:
        console_logger.setLevel('DEBUG')
    else:
        console_logger.setLevel('INFO')

    return console_logger