##
# SALTYSSH
#
# Copyright (c) 2022 by Duck Hunt-Pr0
# Free software under the terms of the LGPL v3.0 <https://www.gnu.org/licenses/lgpl-3.0.html>
#
##
import asyncio
import logging
import socket
from ipaddress import IPv4Address
from random import uniform
from time import time as timestamp
from expiringdict import ExpiringDict
from functools import cached_property

from . import SSHD_IDENT_STRING
from .config import Config
from .iplookup import geoIPlookup_Local as geoIPlookup
from . import garblegen

try:
    from . import SSHD_IDENT_STRINGS
except ImportError:
    pass

## socket writebuffer limits

WBUFFR_LIM_L = 1024 * 1  # def. 16384
WBUFFR_LIM_H = 1024 * 4  # def. 65536


class ConnectionCounter:
    "keeps track of current number of connections"

    def __init__(self):
        self._num: int = 0

    def insert(self):
        self._num += 1
        return

    def remove(self):
        self._num -= 1
        return

    def get(self):
        return int(self._num)


def SaltySSHDconnection(config: Config, log: logging.Logger):
    log.info("Ready for connections!")
    connections_counter = ConnectionCounter()

    _cache_IPlookup_dict = dict()
    _cache_IPlookup = ExpiringDict(max_len=1000, max_age_seconds=60, items=_cache_IPlookup_dict)
    log.debug("""Init. a 'ExpiringDict' cache for ip/countries using = '(max_len=1000, max_age_seconds=60)'""")

    @asyncio.coroutine
    async def handle_connection(reader, writer):
        writer.transport.set_write_buffer_limits(high=WBUFFR_LIM_H, low=WBUFFR_LIM_L)  # lower write buffer

        addr = writer.get_extra_info('peername')    #get connecting ip/port
        connections_counter.insert()

        ### geoiplookup
        ## dont do geoIPlookup of if ip & country already exist in cache
        if _cache_IPlookup.get( str(addr[0]) ) == None:
            country:str = geoIPlookup(addr[0])
            _cache_IPlookup[str(addr[0])] = str(country)
            log.debug("""Did geoiplookup for '{ip}'. Received country='{cn}'""".format(ip=addr[0], cn=country))
        else:
            country:str = _cache_IPlookup[str(addr[0])]
            log.debug("""Found '{ip}' in cache with country='{cn}'""".format(ip=addr[0],cn=country))
        ###

        num_bytes_total: int = 0
        buffr_written: int = 0
        log.info("CONNECT| {ip}:{port} cntry='{cntry}'".format(ip=addr[0],
                                                               port=addr[1],
                                                               cntry=country))

        log.info("connection {con}".format(con=connections_counter.get()))
        start_timestamp = timestamp()


        # welcome = 'Saye'
        # for bite in welcome:
        # writer.write(bytes(bite, 'UTF-8'))
        # await asyncio.sleep(1)  # todo: use config.get*() for config values
        # await writer.drain()


        if config._static_ident is True:
            ident_string = SSHD_IDENT_STRING
        else:
            ident_string = garblegen.getRandIdentStr()

        # writer.write(bytes(ident_string + ' ', 'UTF-8'))

        end_timestamp = timestamp()
        # log.info("SEND| Ident string '{identstr}' to ip={ip}:{port} cntry='{cntry}'".format(identstr=ident_string, ip=addr[0], port=addr[1],cntry=country))

        buffr_drain_limit: int = 32  # WBUFFR_LIM_L  # run writer.drain() when passing this(?)

        while True:
            log.debug("Transport buffer {s} / L{0}l H{1}".format(*writer.transport.get_write_buffer_limits(),
                                                                 s=writer.transport.get_write_buffer_size()))
            # to_send = bytes('chingchong ', 'UTF-8')  # '\033[s' + '\033[u'
            to_send = bytes(garblegen.getRandStr(8, 12), 'UTF-8')  # '\033[s' + '\033[u'
            num_bytes = len(to_send)

            log.debug("PAYLOAD| ip={ip}:{port} payload=\n\n{plod}\n".format(plod=to_send,
                                                                            ip=addr[0],
                                                                            port=addr[1],
                                                                            num_bytes=len(to_send)))

            try:
                writer.write(to_send)
                log.info(
                    "SEND| ip={ip}:{port} byt={sendtbytes:d} totbyt={totalbytes:d} twasted={waste:.3f} cntry='{cntry}'".format(
                        ip=addr[0],
                        port=addr[1],
                        sendtbytes=num_bytes,
                        totalbytes=num_bytes_total,
                        waste=(end_timestamp - start_timestamp),
                        cntry=country
                    ))
                buffr_written += num_bytes

                # if buffr_written >= buffr_drain_limit:
                if True:  # todo: not using drain() each time cause of socket.send() exceptions? enable for now
                    log.debug("Doing writer.drain()")
                    await writer.drain()
                    buffr_written = 0

            except BrokenPipeError:
                log.debug("Broken pipe to {ip} {port}".format(ip=addr[0], port=addr[1]))
                break
            except ConnectionResetError:
                log.debug("Connection closed by {ip}:{port}".format(ip=addr[0], port=addr[1]))
                break
            except InterruptedError:
                log.debug("Socket.send() exception?")

            end_timestamp = timestamp()
            num_bytes_total += num_bytes

            if config._speed_do_random == True:
                sleep_time = uniform(config._speed_min, config._speed_max)  # todo: use config.get*() for config values
            else:
                sleep_time = config._speed_min

            log.debug("Sleeping {s:.3f}s for ip={ip}:{port} cntry='{cntry}'".format(s=sleep_time,
                                                                                    ip=addr[0],
                                                                                    port=addr[1],
                                                                                    cntry=country)
                      )
            await asyncio.sleep(round(float(sleep_time)))  # todo: use config.get*() for config values

        end_timestamp = timestamp()
        wasted_time = end_timestamp - start_timestamp
        log.info("CLOSE| ip={ip}:{port} totbyt={totalbytes:d} totwasted={waste:.3f} cntry='{cntry}'".format(ip=addr[0],
                                                                                                            port=addr[
                                                                                                                1],
                                                                                                            waste=wasted_time,
                                                                                                            totalbytes=num_bytes_total,
                                                                                                            cntry=country

                                                                                                            ))
        connections_counter.remove()
        # await writer.drain()
        writer.close()

        # except ConnectionResetError:
        #     print("Connection lost!")
        #
        # finally:
        #     try:

        # print('Server done')
        # except RuntimeError:
        #     print("jalla!")

    loop = asyncio.get_event_loop()
    log.debug("AsyncIO event loop set..")
    coro = asyncio.start_server(handle_connection,
                                host=config._listen_ip,
                                port=config._listen_port,
                                loop=loop,
                                family=socket.AF_UNSPEC)
    log.debug("AsyncIO coroutine set..")

    log.debug("AsyncIO event loop server set..")
    server = loop.run_until_complete(coro)
    log.debug("AsyncIO event loop server complete..")

    # print('Serving on {}'.format(server.sockets[0].getsockname()))

    try:
        log.debug("AsyncIO event loop run forever..")
        loop.run_forever()
    except KeyboardInterrupt:
        log.info("QUIT| CTRL-C Detected! Quitting...")
        log.debug("AsyncIO event loop stopping..")
        loop.stop()
        log.debug("AsyncIO event loop stopped..")
    # while loop.is_running():
    #    pass
    # loop.close()

# SaltySSHDserver = multiprocessing.Process(target=SaltySSHDconnection, name='SaltySSHD')
