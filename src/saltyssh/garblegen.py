from random import randint
from string import ascii_letters, digits, punctuation, octdigits
from time import sleep
try:
    from . import SSHD_IDENT_STRINGS
except ImportError:
    pass


# todo: THIS FILE IS A FUCKING MESS...

# winniepooh = r"""
#                     .--,
#      .-.    __,,,__/    |
#     /   \-'`        `-./_
#     |    |               `)
#      \   `             `\ ;
#     /       ,        ,    |
#     |      /         : O /_
#     |          O  .--;__   '.
#     |                (  )`.  |
#     \                 `-` /  |
#      \          ,_  _.-./`  /
#       \          \``-.(    /
#       |           `---'   /--.
#     ,--\___..__        _.'   /--.
# jgs \          `-._  _`/    '    '.
#     .' ` ' .       ``    '        ."""

extras = ['\U0001F61B', '\U0001F600', '\u1F609', '\u1F60E']

# https://tldp.org/HOWTO/Bash-Prompt-HOWTO/x361.html
# '\033[1A','\033[2A','\033[1B','\033[1B'
whites = ['\n', ' ', '', '\033[s', '\033[u', '\033[3D', '\033[3C', '\033[K', '\033[s', '\033[u']

chars = '' + ascii_letters + digits + punctuation + octdigits
# chars = "".join(chars)
chars = list(chars) + whites


def getRandIdentStr():
    ident = SSHD_IDENT_STRINGS[randint(0, len(SSHD_IDENT_STRINGS) - 1)]
    return ident


def getRandStr(len_min: int = 8, len_max: int = 32, character_sets=ascii_letters + digits + punctuation):
    rand_str=''
    charset = '' + character_sets
    charset = list(charset) + [' ', '', '\033[1D'] + [' logi', 'Passwor', 'root']
    ['4.15.0-72-generic', '7.58.0-2ubuntu'] + ['Last login', 'r00t', 'uptime']
    # + ['SsH'] \
    # + ['\033[1A', '\033[2A', '\033[1B', '\033[1B'] \
    # + ['\033[K'] \
    # + ['\033[2J']

    for i in range(randint(len_min, len_max)):
        rand_str += charset[randint(1, len(charset) - 1)]

    return rand_str




