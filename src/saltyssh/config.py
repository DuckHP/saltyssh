##
# SALTYSSH
#
# Copyright (c) 2022 by Duck Hunt-Pr0
# Free software under the terms of the LGPL v3.0 <https://www.gnu.org/licenses/lgpl-3.0.html>
##
import logging

from . import __version__
import argparse
import configparser
import textwrap


# class IPConfig(argparse._AppendAction):
#     """the listen ip and port from the argparser --listen argument"""
#
#     def __call__(self, parser, namespace, values, option_string):
#         ip = values[0]  # todo: do some checking for valid ip here?
#         port = values[1]  # todo: check for valid port?
#         d = {"ip": ip, "port": values[1]}
#         return super().__call__(parser, namespace, d, option_string)


class Config:
    def __init__(self,arguments:argparse.Namespace,logger:logging.Logger):
        self._config_file=None
        self._config=configparser.ConfigParser()
        ## cfg
        self._listen_ip:str
        self._listen_port:int
        self._static_ident:bool
        self._debug:bool
        self._speed_do_random:bool
        self._speed_min:float
        self._speed_max:float
        self._geoip_enabled:bool
        self._geoip_use_local:bool
        self._geoip_url:str
        ## cfg
        self.log = logger

        self._do_arguments(arguments)

    def _do_arguments(self,arguments):
        #print(arguments)

        if arguments.config != None:
            self.log.debug("Loading config file..")
            self._config_file=arguments.config
            self._do_configfile()
        elif arguments.ip_port != None:
            self.log.debug("Configure listen ip:port..")
            self._listen_ip=str(arguments.ip_port[0])
            self._listen_port = str(arguments.ip_port[1])
            print(self._listen_ip,self._listen_port)

    def _do_configfile(self):

        try:
           self._config.read_file( self._config_file )
           self._config_file.close()
        except IOError:
            self.log.critical("Could not open in file!!")


        self._listen_ip = self._config['server']['listen_ip']
        self._listen_port = self._config.getint('server','listen_port')
        self._static_ident = self._config['server']['static_ident']

        self._speed_do_random = self._config.getboolean('speed','speed_do_random')
        self._speed_min = self._config.getfloat('speed','speed_min')
        if self._speed_min < 1.0: self._speed_min=1.0
        self._speed_max = self._config.getfloat('speed','speed_max')
        if self._speed_max > self._speed_min: self._speed_max=self._speed_min

        self._geoip_enabled = self._config.getboolean('geoip','enabled')
        self._geoip_use_local = self._config.getboolean('geoip', 'use_local')
        self._geoip_url = self._config.get('geoip', 'geoip_url')


parser = argparse.ArgumentParser(
    prog='saltyssh',
    usage='%(prog)s (--listen IP PORT | --config [configfile])',
    epilog=None,
    add_help=False,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description='''%(prog)s v{ver} - Copyright (c) 2022 by Duck Hunt-Pr0\n\nA tarpit to make wannabe SSH hackers salty. Inspired by Chris 'skeeto' Wellons' "endlessh".\n\n%(prog)s is free software under the terms of the LGPL v3.0'''.format(
        ver=__version__)
)

# fix argparser calling the arguments "optionals"
parser._optionals.title = 'arguments'

parser_grp_excl = parser.add_mutually_exclusive_group(required=True)
parser_grp_excl.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
                             help='Show this help message and exit')
parser_grp_excl.add_argument('-l', '--listen', nargs=2, dest='ip_port',help='The IP and port to listen to', metavar=("IP", "PORT"))
parser_grp_excl.add_argument('-c', '--config', type=argparse.FileType('r'), help='Path to a saltyssh *.ini config file')
parser_grp_excl.add_argument('-v', '--version', action='version', version='%(prog)s {ver}'.format(ver=__version__),
                             help='Show version and exit')
parser.add_argument('-V', '--verbose', action='count', help='Show more stuff. Repeatable [-VVV]',default=0)

