"""
SaltySSH - A SSH tarpit whatfor dicking around with wannabe ssh h4x0rs and scanners
"""
##
# SALTYSSH
#
# Copyright (c) 2022 by Duck Hunt-Pr0 <https://gitlab.com/DuckHP/saltyssh>
# Free software under the terms of the LGPL v3.0 <https://www.gnu.org/licenses/lgpl-3.0.html>
#
##

__version__: str = '2.0'
__package__: str = 'saltyssh'
__license__: str = 'GNU Lesser General Public License v3.0'

LOGGER_DEFAULT_LVL: str = 'INFO'

CONFIG_DEFAULT_FILE_NAME: str = 'saltyssh.ini'
CONFIG_DEFAULT_FILE_PATH: list = ['./', '/etc/saltyssh/', '~/.config/saltyssh/']  # todo: check/mkdir somewhere

SSHD_DEFAULT_IF: str = '0.0.0.0'
SSHD_DEFAULT_PORT: int = 20202
SSHD_RANDOMIZED_SPEED: bool = False  # if false, use SSHD_DEFAULT_MIN_SPEED
SSHD_DEFAULT_MIN_SPEED: float = 5.0  # min s between sending shit
SSHD_DEFAULT_MAX_SPEED: float = 8.0  # max s between sending shit

SSHD_IDENT_STRING:str = 'SSH-2.0-OpenSSH_8.8'   # todo: deprecated ATM. writer.write() being done before ssh banner
SSHD_IDENT_STATIC:str = True
# # note: https://github.com/0x4D31/hassh-utils/blob/master/hasshdb
SSHD_IDENT_STRINGS: list = ['SSH-2.0-OpenSSH_5.9p1 Debian-5ubuntu1.1',
                            'SSH-2.0-ssh2js0.0.18',
                            'SSH-2.0-OpenSSH_8.8',
                            'SSH-2.0-',
                            'SSH-1.99-OpenSSH_3.9p1',
                            'SSH-2.0-OpenSSH_7.4p1 Raspbian-10+deb9u2',
                            'SSH-1.5-',
                            'SSH-1.0-lshd-0.5b a GNU ssh',
                            'SSH-2.0-dropbear_0.51',
                            'SSH-2.0-wolfSSHv 1.4.8',
                            'SSH-1.99-OpenSSH_3.9p1',
                            'SSH-2.0-Cisco-1.25',
                            'SSH-2.0-RebexSSH_1.0.2.27069',
                            'SSH-2.0-Trendchip_0.1',
                            'SSH-1.99-BDCOMSSH_2.0.0',
                            'SSH-2.0-OpenSSH_5.9 NetBSD_Secure_Shell-20110907-hpn13v11-lpk',
                            'SSH-2.0-HUAWEI-1.5']

HTTP_USER_AGENT: str = 'saltyssh/{version} ({url})'  # .format(url='gitlab.com/DuckHP/saltyssh', version=__version__)
HTTP_TIMEOUT: int = 5  # s before giving up http iplookup requests

GEOIP_LOOKUP_USE: bool = True
GEOIP_LOOKUP_USE_LOCAL: bool = False
GEOIP_LOOKUP_LOCAL_BIN_DETECT: bool = True
GEOIP_LOOKUP_LOCAL_BIN: str = 'geoiplookup'
GEOIP_LOOKUP_LOCAL_BIN_PATHS:list = ['/usr/bin',
                                     '/usr/local/sbin',
                                     '/usr/local/bin']
GEOIP_LOOKUP_API_URLS: list = ['https://ip2c.org/?ip={ip}',
                               # https://api.hackertarget.com/geoip/?q={ip} <-- require membership
                               'freegeoip.net/xml/{ip}']
GEOIP_LOOKUP_DO_CACHE: bool = True


# ASCII_FUN_FOLDER = ''   # a folder with some fun ascii textfiles to sometimes use
