#!/usr/bin/env python3
##
# SALTYSSH
#
# Copyright (c) 2022 by Duck Hunt-Pr0
# Free software under the terms of the LGPL v3.0 <https://www.gnu.org/licenses/lgpl-3.0.html>
##
##
# notes:
# https://gist.github.com/messa/22398173f039d1230e32
# https://docs.python.org/3/library/asyncio.html
# https://docs.python.org/3/library/asyncio-dev.html
# https://github.com/openssh/openssh-portable/blob/d9dbb5d9a0326e252d3c7bc13beb9c2434f59409/kex.c#L1321
# https://github.com/openssh/openssh-portable/blob/2dc328023f60212cd29504fc05d849133ae47355/ssh.h#L103
# https://docs.python.org/3/library/asyncio-protocol.html#asyncio.WriteTransport.set_write_buffer_limits
# https://www.rfc-editor.org/rfc/rfc4253.html#section-4
# https://tldp.org/HOWTO/Bash-Prompt-HOWTO/x361.html
# https://datatracker.ietf.org/doc/html/rfc1184
# https://www2.math.upenn.edu/~kazdan/210/computer/ansi.html
# https://www.asciitable.com/
# https://github.com/openssh/openssh-portable/blob/2dc328023f60212cd29504fc05d849133ae47355/ssherr.h
# https://github.com/Freaky/tarssh/tree/master/src
# https://github.com/mailgun/expiringdict#usage
# https://www.thepythoncode.com/article/manipulate-ip-addresses-using-ipaddress-module-in-python
##

import multiprocessing
from .config import Config as SaltyConfig
from .config import parser as SaltyArgs
from .logger import makeLogger
from .server import SaltySSHDconnection


def main():
    """Run the shit.."""
    cli_arguments = SaltyArgs.parse_args()
    # print(cli_arguments)

    log = makeLogger(cli_arguments)
    if cli_arguments.verbose == 0:
        log.setLevel('INFO')
    elif cli_arguments.verbose == 1:
        log.setLevel('VERBOSE')
    elif cli_arguments.verbose >= 2:
        log.setLevel('DEBUG')

    log.info("SaltySSH starting")
    config = SaltyConfig(cli_arguments, log)

    log.debug("Multiprocessing initalizing")

    server_process = multiprocessing.Process(target=SaltySSHDconnection(config=config,
                                                                        log=log),
                                             name='SaltySSHD')
    log.debug("Multiprocessing stopped")
    # server_process.run()
    try:
        server_process.terminate()
    except AttributeError:
        pass

    try:
        server_process.join()
    except AssertionError:
        pass


if __name__ == '__main__':
    # logging.getLogger('asyncio').setLevel('DEBUG')
    # logging.basicConfig(level=logging.DEBUG)
    main()
