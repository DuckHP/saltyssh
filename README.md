# SaltySSH 2.0

## About SaltySSH

SaltySSH is a tarpit intended to annoy wannabe SSH hackers/scanners. Heavily inspired by Chris _skeeto_ Wellons' [endlessh](https://github.com/skeeto/endlessh).

## Requirements
* Python 3(.9)
* [expiringdict](https://github.com/mailgun/expiringdict) (1.2.1)
* [geoiplookup](https://www.maxmind.com/app/c) _(check your linux distros' repositories)_
  * or using http(s) calls to some geoip-lookup online api _(see sourcecode and/or *.ini file)_

## Running from source in virtualenv

Running it from source, using _.ini_ config file:
```
$ virtualenv [-p python3.9] venv
$ source venv/bit/activate
$ pip install -r requirements.txt
$ cp docs/saltyssh.ini ./
```

Edit `./saltyssh.ini` according to need.

Then, start it by running:
```
$ PYTHONPATH="${PYTHONPATH}:$PWD/src" python -m saltyssh -c $PWD/docs/saltyssh.ini -VV
```

## Config example

` `


## License

Distributed under the LGPL 3.0 License. See `LICENSE.txt` for more information.